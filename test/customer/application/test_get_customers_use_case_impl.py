import unittest
from unittest.mock import MagicMock

from src.customer.application.usecase.get_customers_use_case_impl import GetCustomersUseCaseImpl
from src.customer.domain.model.customer import Customer
from src.customer.domain.model.customer_basket import CustomerBasket
from src.customer.domain.model.receipt import CustomerReceipt


class TestGetCustomersUseCase(unittest.TestCase):
    def setUp(self):
        self.mock_customer_spi = MagicMock()
        self.mock_customer_basket_spi = MagicMock()

        self.get_customers_use_case_impl = GetCustomersUseCaseImpl(
            self.mock_customer_spi, self.mock_customer_basket_spi
        )

    def test_execute_should_return_filtered_list_of_customers(self):
        # GIVEN
        customers_data = [
            Customer(id=1, firstname="John", lastname="Doe", email="john.doe@example.com", active=True),
            Customer(id=2, firstname="Jane", lastname="Smith", email="jane.smith@example.com", active=True),
            Customer(id=2, firstname="Richard", lastname="Doe", email="richard.doe@example.com", active=False),
        ]
        self.mock_customer_spi.get_all_customers.return_value = customers_data

        customer_baskets_data = [
            CustomerBasket(id=1, user_id=2, price=1500.2),
        ]
        self.mock_customer_basket_spi.get_all_customer_baskets.return_value = customer_baskets_data

        # WHEN
        result = self.get_customers_use_case_impl.execute()

        # THEN
        expected_result = [CustomerReceipt(1, 2, "Smith", "jane.smith@example.com", 1500.2)]

        self.assertEqual(result, expected_result)
        self.mock_customer_spi.get_all_customers.assert_called_once()
        self.mock_customer_basket_spi.get_all_customer_baskets.assert_called_once()

    def test_execute_should_return_all_of_customers(self):
        # GIVEN
        customers_data = [Customer(id=1, firstname="John", lastname="Doe", email="john.doe@example.com", active=True)]
        self.mock_customer_spi.get_all_customers.return_value = customers_data

        customer_baskets_data = [
            CustomerBasket(id=1, user_id=1, price=100),
        ]
        self.mock_customer_basket_spi.get_all_customer_baskets.return_value = customer_baskets_data

        # WHEN
        result = self.get_customers_use_case_impl.execute()

        # THEN
        expected_result = [CustomerReceipt(1, 1, "Doe", "john.doe@example.com", 100)]
        self.assertEqual(result, expected_result)
