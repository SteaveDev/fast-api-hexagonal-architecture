import unittest
from unittest.mock import MagicMock

from sqlalchemy.orm import Session

from src.customer.domain.model.customer import Customer
from src.customer.infrastructure.secondary.persistence.customer.customer_adapter import CustomerAdapter
from src.customer.infrastructure.secondary.persistence.customer.customer_entity import CustomerEntity


class TestCustomerAdapter(unittest.TestCase):
    def setUp(self):
        # Créer une session SQLAlchemy mock pour les tests
        self.mock_session = MagicMock(spec=Session)

        # Initialiser la classe CustomerAdapter avec la session mock
        self.customer_adapter = CustomerAdapter(session=self.mock_session)

    def test_get_all_customers(self):
        # Créer des entités de client mock pour simuler la réponse de la base de données
        mock_customer_entities = [
            CustomerEntity(id=1, firstname="John", lastname="Doe", email="john.doe@example.com", active=True),
            CustomerEntity(id=2, firstname="Jane", lastname="Doe", email="jane.doe@example.com", active=False),
        ]

        # Configurer le comportement de la session mock pour retourner les entités mock
        self.mock_session.query.return_value.all.return_value = mock_customer_entities

        # Appeler la méthode à tester
        result = self.customer_adapter.get_all_customers()

        # Vérifier que la méthode de session mock a été appelée correctement
        self.mock_session.query.assert_called_once_with(CustomerEntity)
        self.mock_session.query.return_value.all.assert_called_once()

        # Vérifier que le résultat est une liste d'objets Customer
        self.assertIsInstance(result, list)
        self.assertEqual(len(result), 2)
        self.assertIsInstance(result[0], Customer)
        self.assertIsInstance(result[1], Customer)

        self.assertEqual(result[0],
                         Customer(id=1, firstname="John", lastname="Doe", email="john.doe@example.com", active=True))
        self.assertEqual(result[1],
                         Customer(id=2, firstname="Jane", lastname="Doe", email="jane.doe@example.com",
                                  active=False))
