import unittest
from unittest.mock import MagicMock

from sqlalchemy.orm import Session

from src.customer.domain.model.customer_basket import CustomerBasket
from src.customer.infrastructure.secondary.persistence.basket.customer_basket_adapter import CustomerBasketAdapter
from src.customer.infrastructure.secondary.persistence.basket.customer_basket_entity import CustomerBasketEntity


class TestCustomerBasketAdapter(unittest.TestCase):
    def setUp(self):
        # Créer une session SQLAlchemy mock pour les tests
        self.mock_session = MagicMock(spec=Session)

        # Initialiser la classe CustomerBasketAdapter avec la session mock
        self.customer_basket_adapter = CustomerBasketAdapter(session=self.mock_session)

    def test_get_all_customer_baskets(self):
        # Créer des entités de panier client mock pour simuler la réponse de la base de données
        mock_customer_basket_entities = [
            CustomerBasketEntity(id=1, user_id=1, price=50.0),
            CustomerBasketEntity(id=2, user_id=2, price=75.0),
        ]

        # Configurer le comportement de la session mock pour retourner les entités mock
        self.mock_session.query.return_value.all.return_value = mock_customer_basket_entities

        # Appeler la méthode à tester
        result = self.customer_basket_adapter.get_all_customer_baskets()

        # Vérifier que la méthode de session mock a été appelée correctement
        self.mock_session.query.assert_called_once_with(CustomerBasketEntity)
        self.mock_session.query.return_value.all.assert_called_once()

        # Vérifier que le résultat est une liste d'objets CustomerBasket
        self.assertIsInstance(result, list)
        self.assertEqual(len(result), 2)
        self.assertIsInstance(result[0], CustomerBasket)
        self.assertIsInstance(result[1], CustomerBasket)

        # Ajouter des assertions spécifiques sur les valeurs des objets CustomerBasket si nécessaire
        self.assertEqual(result[0].user_id, 1)
        self.assertEqual(result[1].price, 75.0)
