from fastapi.testclient import TestClient

from main import app

client = TestClient(app)


def test_get_customers_endpoint():
    # GIVEN
    response = client.get("/customers")

    assert response.status_code == 200

    assert len(response.json()) > 0

    for customer_receipt in response.json():
        assert "id" in customer_receipt
        assert "user_id" in customer_receipt
        assert "lastname" in customer_receipt
        assert "email" in customer_receipt
        assert "price" in customer_receipt
