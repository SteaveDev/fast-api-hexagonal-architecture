# Hexagonal architecture (FastApi)

```bash
docker compose start
```

```bash
psql --host=localhost --port=5431 --username=admin -d shopping

# password : 123456
```

```sql
INSERT INTO customer
VALUES (1, 'John', 'Doe', 'john.doe@ippon.fr', true);

INSERT INTO customer
VALUES (2, 'Jane', 'Doe', 'jane.doe@ippon.fr', false);

INSERT INTO customer
VALUES (3, 'Richard', 'Doe', 'richard.doe@ippon.fr', true);

INSERT INTO customer_basket
VALUES (1, 1, 123.0);

INSERT INTO customer_basket
VALUES (2, 2, 153.0);

INSERT INTO customer_basket
VALUES (3, 3, 223.0);
```