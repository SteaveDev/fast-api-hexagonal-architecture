from sqlalchemy import Column, Integer, Float
from sqlalchemy.orm import declarative_base

Base = declarative_base()


class CustomerBasketEntity(Base):
    __tablename__ = "customer_basket"

    id = Column(Integer, primary_key=True, index=True)
    user_id = Column(Integer, unique=True, index=True)
    price = Column(Float, index=True, nullable=True)
