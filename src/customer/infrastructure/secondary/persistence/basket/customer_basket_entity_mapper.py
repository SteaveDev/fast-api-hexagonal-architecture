from src.customer.domain.model.customer_basket import CustomerBasket
from src.customer.infrastructure.secondary.persistence.basket.customer_basket_entity import CustomerBasketEntity


class CustomerBasketEntityMapper:
    @staticmethod
    def to_model(customer_basket_entity: CustomerBasketEntity) -> CustomerBasket:
        return CustomerBasket(
            id=customer_basket_entity.id,
            user_id=customer_basket_entity.user_id,
            price=customer_basket_entity.price,
        )

    @staticmethod
    def to_entity(customer_basket_model: CustomerBasket) -> CustomerBasketEntity:
        return CustomerBasketEntity(
            id=customer_basket_model.id,
            firstname=customer_basket_model.user_id,
            lastname=customer_basket_model.price
        )
