from sqlalchemy.orm import Session

from src.customer.domain.model.customer_basket import CustomerBasket
from src.customer.domain.port.customer_basket_spi import CustomerBasketSpi
from src.customer.infrastructure.secondary.persistence.basket.customer_basket_entity import CustomerBasketEntity
from src.customer.infrastructure.secondary.persistence.basket.customer_basket_entity_mapper import \
    CustomerBasketEntityMapper


class CustomerBasketAdapter(CustomerBasketSpi):
    def __init__(self, session: Session):
        self.session = session

    def get_all_customer_baskets(self) -> list[CustomerBasket]:
        customer_basket_entities = self.session.query(CustomerBasketEntity).all()

        customer_baskets = [CustomerBasketEntityMapper.to_model(entity) for entity in customer_basket_entities]

        return customer_baskets
