from src.customer.domain.model.customer import Customer

from src.customer.infrastructure.secondary.persistence.customer.customer_entity import CustomerEntity


class CustomerEntityMapper:
    @staticmethod
    def to_model(customer_entity: CustomerEntity) -> Customer:
        return Customer(
            id=customer_entity.id,
            firstname=customer_entity.firstname,
            lastname=customer_entity.lastname,
            email=customer_entity.email,
            active=customer_entity.active
        )

    @staticmethod
    def to_entity(customer_model: Customer) -> CustomerEntity:
        return CustomerEntity(
            id=customer_model.id,
            firstname=customer_model.firstname,
            lastname=customer_model.lastname,
            email=customer_model.email,
            active=customer_model.active
        )
