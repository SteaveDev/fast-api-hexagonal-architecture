from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from src.customer.infrastructure.secondary.persistence.customer.customer_entity import Base

SQLALCHEMY_DATABASE_URL = "postgresql://admin:123456@localhost:5431/shopping"
engine = create_engine(SQLALCHEMY_DATABASE_URL)

Base.metadata.create_all(bind=engine)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)


def repository():
    postgresql_database = SessionLocal()
    try:
        yield postgresql_database
    finally:
        postgresql_database.close()
