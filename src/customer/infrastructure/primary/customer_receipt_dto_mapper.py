from src.customer.domain.model.receipt import CustomerReceipt
from src.customer.infrastructure.primary.customer_receipt_dto import CustomerReceiptDto


class CustomerMapper:
    @staticmethod
    def to_dto(customer_receipt: CustomerReceipt) -> CustomerReceiptDto:
        return CustomerReceiptDto(
            id=customer_receipt.id,
            user_id=customer_receipt.user_id,
            lastname=customer_receipt.lastname,
            email=customer_receipt.email,
            price=customer_receipt.price
        )
