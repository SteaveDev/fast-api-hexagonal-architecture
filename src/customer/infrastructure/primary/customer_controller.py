from fastapi import APIRouter, Depends

from src.customer.domain.usecase.get_customers_use_case import GetCustomersUseCase
from src.customer.infrastructure.configuration.configuration import get_customers_use_case_config
from src.customer.infrastructure.primary.customer_receipt_dto import CustomerReceiptDto
from src.customer.infrastructure.primary.customer_receipt_dto_mapper import CustomerMapper

customer_router = APIRouter()


@customer_router.get('/customers', response_model=list[CustomerReceiptDto])
def get_customers(get_customer_use_case: GetCustomersUseCase = Depends(get_customers_use_case_config)):
    customer_receipts = get_customer_use_case.execute()
    customer_receipt_dtos = [CustomerMapper.to_dto(customer_receipt) for customer_receipt in customer_receipts]

    return customer_receipt_dtos
