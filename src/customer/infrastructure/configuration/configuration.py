from fastapi.params import Depends
from sqlalchemy.orm import Session

from src.customer.application.usecase.get_customers_use_case_impl import GetCustomersUseCaseImpl
from src.customer.domain.port.customer_basket_spi import CustomerBasketSpi
from src.customer.domain.port.customer_spi import CustomerSpi
from src.customer.domain.usecase.get_customers_use_case import GetCustomersUseCase
from src.customer.infrastructure.secondary.persistence.basket.customer_basket_adapter import CustomerBasketAdapter
from src.customer.infrastructure.secondary.persistence.customer.customer_adapter import CustomerAdapter
from src.customer.infrastructure.secondary.persistence.database import repository


def customer_spi_config(session: Session = Depends(repository)) -> CustomerSpi:
    return CustomerAdapter(session)


def customer_basket_spi_config(session: Session = Depends(repository)) -> CustomerBasketSpi:
    return CustomerBasketAdapter(session)


def get_customers_use_case_config(customer_spi: CustomerSpi = Depends(customer_spi_config),
                                  customer_basket_spi: CustomerBasketSpi = Depends(
                                      customer_basket_spi_config)) -> GetCustomersUseCase:
    return GetCustomersUseCaseImpl(customer_spi, customer_basket_spi)
