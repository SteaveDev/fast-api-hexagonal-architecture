class Customer:
    def __init__(self, id: int, firstname: str, lastname: str, email: str, active: bool):
        self.id = id
        self.firstname = firstname
        self.lastname = lastname
        self.email = email
        self.active = active

    def __eq__(self, other):
        if not isinstance(other, Customer):
            return False
        return (
                self.id == other.id and
                self.firstname == other.firstname and
                self.lastname == other.lastname and
                self.email == other.email and
                self.active == other.active
        )
