from typing import List

from src.customer.domain.model.receipt import CustomerReceipt
from src.customer.domain.port.customer_basket_spi import CustomerBasketSpi
from src.customer.domain.port.customer_spi import CustomerSpi
from src.customer.domain.usecase.get_customers_use_case import GetCustomersUseCase


class GetCustomersUseCaseImpl(GetCustomersUseCase):
    def __init__(self, customer_spi: CustomerSpi, customer_basket_spi: CustomerBasketSpi):
        self.customer_spi = customer_spi
        self.customer_basket_spi = customer_basket_spi

    def execute(self) -> List[CustomerReceipt]:
        customers = self.customer_spi.get_all_customers()
        active_customers = [customer for customer in customers if customer.active]

        customer_baskets = self.customer_basket_spi.get_all_customer_baskets()
        customer_receipts = []
        for active_customer in active_customers:
            for customer_basket in customer_baskets:
                if customer_basket.user_id == active_customer.id:
                    customer_receipt = CustomerReceipt(
                        id=customer_basket.id,
                        user_id=active_customer.id,
                        lastname=active_customer.lastname,
                        email=active_customer.email,
                        price=customer_basket.price
                    )
                    customer_receipts.append(customer_receipt)
                    break

        return customer_receipts
