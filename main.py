from fastapi import FastAPI

from src.customer.infrastructure.primary.customer_controller import customer_router

app = FastAPI()

app.include_router(customer_router, prefix="", tags=["customers"])
